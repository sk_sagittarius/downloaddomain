﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DownloadDomain
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DownloadButtonClick(object sender, RoutedEventArgs e)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadProgressChanged += Done;
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(MessageShow);
                if (File.Exists("testfile.zip"))
                {
                    File.Delete("testfile.zip");
                    client.DownloadFileAsync(new System.Uri("http://212.183.159.230/5MB.zip"), "testfile.zip");
                }
                else
                {
                    client.DownloadFileAsync(new System.Uri("http://212.183.159.230/5MB.zip"), "testfile.zip");
                }
            }
        }

        private void Done(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void MessageShow(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show($"Done!");
        }
    }
}
